import React from "react";
import avatar from "../../img/avatar.svg"
import search from "../../img/search.svg"
import dashboard from "../../img/dashboard.svg"
import revenue from "../../img/revenue.svg"
import notif from "../../img/notif.svg"
import analytics from "../../img/analytics.svg"
import inventor from "../../img/inventor.svg"
import logout from "../../img/logout.svg"
import mode from "../../img/mode.svg"
import lun from "../../img/lun.svg"
import "../style/style.css"

function Login (){
    return (
        <>
        <div className="login-data">
            <img src = {avatar} alt =""></img>
            <div >
                <div>AnimatedFred</div>
                <div>animated@demo.com</div>
            </div>
        </div>
        <div className="menu">
            <div className="login-data">
                    <img src = {search} alt =""></img>
                    <div>Search...</div>
            </div>
            <div className="login-data">
                    <img src = {dashboard} alt =""></img>
                    <div>Dashboard</div>
            </div>
            <div className="login-data">
                    <img src = {revenue} alt =""></img>
                    <div>Revenue</div>
            </div>
            <div className="login-data">
                    <img src = {notif} alt =""></img>
                    <div>Notifications</div>
            </div>
            <div className="login-data">
                    <img src = {analytics} alt =""></img>
                    <div>Analytics</div>
            </div>
            <div className="login-data">
                    <img src = {inventor} alt =""></img>
                    <div>Inventory</div>
            </div>
            </div>
            <div className = "exit-menu">
                <div className="login-data">
                        <img src = {logout} alt =""></img>
                        <div>Logout</div>
                        
                </div>
                <div className="login-data">
                        <img className="mode_lun" src = {mode} alt =""></img>
                        <div className="mode">Light mode</div>
                        <div id="lun"> <img src = {lun} alt =""></img></div>
                </div>
            </div>    
       
        </>
    )
}
export default Login;